package tsb.vocabulary.gui;

import org.junit.Test;
import tsb.vocabulary.gui.util.DocumentFileFilter;

import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DocumentFileFilterTest {

    @Test
    public void accept_whenTxtExtension_returnsTrue() throws Exception {
        DocumentFileFilter fileFilter = new DocumentFileFilter();
        File mockFile = mock(File.class);
        when(mockFile.isFile()).thenReturn(true);
        when(mockFile.getName()).thenReturn("document.txt");

        boolean result = fileFilter.accept(mockFile);

        assertTrue(result);
    }

    @Test
    public void accept_whenDirectory_returnsTrue() throws Exception {
        DocumentFileFilter fileFilter = new DocumentFileFilter();
        File mockFile = mock(File.class);
        when(mockFile.isDirectory()).thenReturn(true);

        boolean result = fileFilter.accept(mockFile);

        assertTrue(result);
    }

    @Test
    public void accept_whenPdfExtension_returnsFalse() throws Exception {
        DocumentFileFilter fileFilter = new DocumentFileFilter();
        File mockFile = mock(File.class);
        when(mockFile.isFile()).thenReturn(true);
        when(mockFile.getName()).thenReturn("fakefile.pdf");

        boolean result = fileFilter.accept(mockFile);

        assertFalse(result);
    }

    @Test
    public void getDescription_whenCalled_returnsCorrectLabel() throws Exception {
        DocumentFileFilter fileFilter = new DocumentFileFilter();
        String expected = "Archivos de Texto";

        String actual = fileFilter.getDescription();

        assertEquals(expected, actual);
    }
}
