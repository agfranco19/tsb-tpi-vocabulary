package tsb.vocabulary.repositories;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;
import tsb.vocabulary.data.VocabularyTerm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIn.isIn;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("test")
public class VocabularyRepositoryImplTest {
    @Autowired
    private VocabularyRepository vocabularyRepository;

    @Ignore
    @Test
    public void getNumberOfDocuments_whenCalled_returnsCorrectNumberOfDocuments() throws Exception {
        int numberOfDocuments = vocabularyRepository.getNumberOfDocuments();

        assertThat(numberOfDocuments, is(4));
    }

    @Ignore
    @Test
    public void insertDocument_whenCalled_insertsADocument() throws Exception {
        Map<String, Integer> words = new HashMap<>();
        vocabularyRepository.saveProcessedDocument(new Document("document-test.txt"), words);
    }

    @Test
    public void existsDocument_whenDocumentExists_returnsTrue() throws Exception {
        boolean exists = vocabularyRepository.existsDocument(new Document("document-1.txt"));

        assertTrue(exists);
    }

    @Test
    public void existsDocument_whenDocumentExists_returnsFalse() throws Exception {
        boolean exists = vocabularyRepository.existsDocument(new Document("non-existing-document.txt"));

        assertFalse(exists);
    }

    @Test
    public void listDocuments_whenCalled_returnsListProcessedDocuments() throws Exception {
        Document expected = new Document("document-1.txt");

        List<Document> documents = vocabularyRepository.listDocuments();

        assertThat(expected, isIn(documents));
    }

    @Test
    public void getVocabulary_whenCalled_returnsAVocabularyWithTermsNotNull() {
        Vocabulary vocabulary = vocabularyRepository.getVocabulary();
        List<VocabularyTerm> VocabularyTerms = vocabulary.getVocabularyTerms();

        assertNotNull(VocabularyTerms);
    }

    @Test
    public void getVocabulary_whenCalled_returnsAVocabularyWithTermsCorrectNumberOfTerms() {
        Vocabulary vocabulary = vocabularyRepository.getVocabulary();
        List<VocabularyTerm> VocabularyTerms = vocabulary.getVocabularyTerms();

        assertThat(VocabularyTerms.size(), is(5));
    }
}
