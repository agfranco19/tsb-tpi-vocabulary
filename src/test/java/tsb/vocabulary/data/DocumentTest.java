package tsb.vocabulary.data;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class DocumentTest {

    @Test
    public void getName_whenNameIsSetByConstructor_returnsCorrectName() throws Exception {
        Document document = new Document("test-document.txt");
        String expected = "test-document.txt";

        String actual = document.getName();

        assertEquals(expected, actual);
    }

    @Test
    public void getName_whenNameIsSetUsingAFile_returnsCorrectName() throws Exception {
        Document document = new Document(new File("test-document.txt"));
        String expected = "test-document.txt";

        String actual = document.getName();

        assertEquals(expected, actual);
    }

    @Test
    public void equals_whenNameIsSetByConstructor_returnsTrue() throws Exception {
        Document document1 = new Document("test-document.txt");
        Document document2 = new Document("test-document.txt");

        boolean result = document1.equals(document2);

        assertTrue(result);
    }

    @Test
    public void equals_whenNameIsSetUsingAFile_returnsTrue() throws Exception {
        Document document1 = new Document("test-document.txt");
        Document document2 = new Document(new File("test-document.txt"));

        boolean result = document1.equals(document2);

        assertTrue(result);
    }

    @Test
    public void equals_whenNullIsPassed_returnsFalse() throws Exception {
        Document document1 = new Document("test-document.txt");
        Document document2 = null;

        boolean result = document1.equals(document2);

        assertFalse(result);
    }

    @Test
    public void equals_whenOtherInstanceIsPassed_returnsFalse() throws Exception {
        Document document1 = new Document("test-document.txt");
        String document2 = "test-document.txt";

        boolean result = document1.equals(document2);

        assertFalse(result);
    }


    @Test
    public void hashCode_whenNameIsSetByConstructor_areEquals() throws Exception {
        Document document1 = new Document("test-document.txt");
        Document document2 = new Document("test-document.txt");

        int hashDocument1 = document1.hashCode();
        int hashDocument2 = document2.hashCode();

        assertEquals(hashDocument1, hashDocument2);
    }

    @Test
    public void hashCode_whenNameIsSetUsingAFile_areEquals() throws Exception {
        Document document1 = new Document("test-document.txt");
        Document document2 = new Document(new File("test-document.txt"));

        int hashDocument1 = document1.hashCode();
        int hashDocument2 = document2.hashCode();

        assertEquals(hashDocument1, hashDocument2);
    }

    @Test
    public void toString_whenCalled_returnsDocumentName() {
        Document document1 = new Document("test-document.txt");

        String name = document1.getName();
        String toString = document1.toString();

        assertEquals(name, toString);
    }
}
