package tsb.vocabulary.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Ariel Franco
 */
public class TermTest {

    @Test
    public void equals_whenNullIsPassed_returnsFalse() throws Exception {
        VocabularyTerm term1 = new VocabularyTerm("software", 1, 1);
        VocabularyTerm term2 = null;

        boolean result = term1.equals(term2);

        assertFalse(result);
    }

    @Test
    public void equals_whenOtherInstanceIsPassed_returnsFalse() throws Exception {
        VocabularyTerm term1 = new VocabularyTerm("software", 1, 1);
        String term2 = "software";

        boolean result = term1.equals(term2);

        assertFalse(result);
    }

    @Test
    public void equals_whenOtherTermWithSameWord_returnsTrue() throws Exception {
        VocabularyTerm term1 = new VocabularyTerm("software", 1, 1);
        VocabularyTerm term2 = new VocabularyTerm("software", 5564, 45);

        boolean result = term1.equals(term2);

        assertTrue(result);
    }

    @Test
    public void hashCode_whenNameIsSetByConstructor_areEquals() throws Exception {
        VocabularyTerm term1 = new VocabularyTerm("software", 1, 1);
        VocabularyTerm term2 = new VocabularyTerm("software", 1, 1);

        int hashDocument1 = term1.hashCode();
        int hashDocument2 = term2.hashCode();

        assertEquals(hashDocument1, hashDocument2);
    }
}
