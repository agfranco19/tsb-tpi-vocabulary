package tsb.vocabulary.services;

import org.junit.Ignore;
import org.junit.Test;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;
import tsb.vocabulary.data.VocabularyTerm;
import tsb.vocabulary.repositories.VocabularyRepository;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Ariel Franco
 */
public class VocabularyServiceImplTest {
    private VocabularyService service;

    @Test
    public void existsDocument_whenCalled_AndExistsDocumentReturnsTrue() throws Exception {
        VocabularyRepository mockRepository = mock(VocabularyRepository.class);
        when(mockRepository.existsDocument(new Document("test-document.txt"))).thenReturn(true);
        service = new VocabularyServiceImpl(mockRepository);

        boolean result = service.existsDocument(new Document("test-document.txt"));

        assertTrue(result);
    }

    @Ignore
    @Test
    public void addDocument_whenCalled_AndExistsDocumentReturnsFalse() throws Exception {
        VocabularyRepository mockRepository = mock(VocabularyRepository.class);
//        when(mockRepository.saveProcessedDocument(new Document("test-document.txt"))).thenReturn(false);
        service = new VocabularyServiceImpl(mockRepository);

//        boolean result = service.addDocument(new Document("test-document.txt"));

//        assertFalse(result);
    }

    @Test
    public void listDocuments_whenCalled_returnsAllProcessedDocuments() throws Exception {
        List<Document> expected = new LinkedList<>();
        expected.add(new Document("test-document-1.txt"));
        expected.add(new Document("test-document-2.txt"));
        expected.add(new Document("test-document-3.txt"));
        VocabularyRepository mockRepository = mock(VocabularyRepository.class);
        when(mockRepository.listDocuments()).thenReturn(expected);
        service = new VocabularyServiceImpl(mockRepository);

        List<Document> actual = service.listProcessedDocuments();

        assertThat(actual, containsInAnyOrder(expected.toArray()));
    }

    @Test
    public void getVocabulary() throws Exception {
        List<VocabularyTerm> expected = new LinkedList<>();
        expected.add(new VocabularyTerm("Software", 2, 1));
        expected.add(new VocabularyTerm("TSB", 5, 2));
        VocabularyRepository mockRepository = mock(VocabularyRepository.class);
        when(mockRepository.getVocabulary()).thenReturn(new Vocabulary(expected));
        service = new VocabularyServiceImpl(mockRepository);

        List<VocabularyTerm> actual = service.getVocabulary().getVocabularyTerms();

        assertThat(actual, containsInAnyOrder(expected.toArray()));
    }
}
