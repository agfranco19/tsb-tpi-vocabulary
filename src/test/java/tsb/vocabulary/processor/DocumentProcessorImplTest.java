package tsb.vocabulary.processor;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DocumentProcessorImplTest {
    private DocumentProcessor documentProcessor;

    @Test
    public void processDocument_whenCalled_andWordExistsReturnsCorrectFrequencyValue() {
        final String document = "En aquella pequeña sociedad tucumana, llena de movimiento, vida e\n" +
                "imaginación, Rejalte cayó como un soplo helado. Las mujeres se\n" +
                "sobrecogieron y los hombres fruncieron el entrecejo. Durante un mes la\n" +
                "sociedad y el vicario se miraron como dos adversarios que se estudian.";
        documentProcessor = createDocumentProcessor(new DocumentScannerFactory() {
        });

        Map<String, Integer> vocabulary = documentProcessor.processDocument(document);
        int frequency = vocabulary.get("SOCIEDAD");

        assertEquals(2, frequency);
    }

    @Test
    public void processDocument_whenCalled_andWordDoesNotExistsReturnsZeroAsFrequencyValue() {
        final String document = "En aquella pequeña sociedad tucumana.";
        documentProcessor = createDocumentProcessor(new DocumentScannerFactory() {
        });

        Map<String, Integer> vocabulary = documentProcessor.processDocument(document);
        Integer frequency = vocabulary.get("NOTEXISTINGWORD");

        assertNull(frequency);
    }

    private static DocumentProcessorImpl createDocumentProcessor(DocumentScannerFactory documentFactory) {
        final String regex = "[^áéñóíúüÁÉÑÓÍÚÜa-zA-Z]";
        final String charset = "ISO-8859-1";
        ProcessorRulesConfiguration configuration = new ProcessorRulesConfigurationImpl(regex, charset);
        return new DocumentProcessorImpl(configuration, documentFactory);
    }
}
