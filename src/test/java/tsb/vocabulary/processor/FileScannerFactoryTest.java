package tsb.vocabulary.processor;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Scanner;

import static org.junit.Assert.assertNotNull;

/**
 * @author Ariel Franco
 */
public class FileScannerFactoryTest {
    private DocumentScannerFactory documentScannerFactory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void createScanner_whenDocumentIsNull_Throws() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Document must be a valid text file.");
        documentScannerFactory = new FileScannerFactory();

        Scanner scanner = documentScannerFactory.createScanner(null);
    }

    @Test
    public void createScanner_whenDocumentIsAnInvalidFileName_Throws() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Document must be a valid text file.");
        documentScannerFactory = new FileScannerFactory();

        Scanner scanner = documentScannerFactory.createScanner("NotExistingFile.txt");
    }

    @Test
    public void createScanner_whenDocumentIsValidFilename_returnsScanner() {
        documentScannerFactory = new FileScannerFactory();

        Scanner scanner = documentScannerFactory.createScanner("src/test/resources/documents/test_doc_utf8.txt");

        assertNotNull(scanner);
    }
}
