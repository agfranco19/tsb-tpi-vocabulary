CREATE TABLE documents
(
  id            INT AUTO_INCREMENT PRIMARY KEY,
  document_name VARCHAR2(45) UNIQUE NOT NULL
);

CREATE TABLE words
(
  id        INT AUTO_INCREMENT PRIMARY KEY,
  word_name VARCHAR2(60) UNIQUE NOT NULL
);

CREATE TABLE vocabulary
(
  word_id     INT NOT NULL,
  document_id INT NOT NULL,
  frequency   INT,
  PRIMARY KEY (word_id, document_id)
);