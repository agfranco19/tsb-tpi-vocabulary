-- documents data for testing
INSERT INTO documents (id, document_name) VALUES (1, 'document-1.txt');
INSERT INTO documents (id, document_name) VALUES (2, 'document-2.txt');
INSERT INTO documents (id, document_name) VALUES (3, 'document-3.txt');
INSERT INTO documents (id, document_name) VALUES (4, 'document-4.txt');

-- words data for testing
INSERT INTO words (id, word_name) VALUES (1, 'HELLO');
INSERT INTO words (id, word_name) VALUES (2, 'WORLD');
INSERT INTO words (id, word_name) VALUES (3, 'TSB');
INSERT INTO words (id, word_name) VALUES (4, 'COLLEGE');
INSERT INTO words (id, word_name) VALUES (5, 'SOFTWARE');

-- vocabulary data for testing
INSERT INTO vocabulary (word_id, document_id, frequency) VALUES (1, 1, 2);
INSERT INTO vocabulary (word_id, document_id, frequency) VALUES (2, 1, 2);
INSERT INTO vocabulary (word_id, document_id, frequency) VALUES (3, 3, 5);
INSERT INTO vocabulary (word_id, document_id, frequency) VALUES (4, 3, 2);
INSERT INTO vocabulary (word_id, document_id, frequency) VALUES (5, 4, 15);
