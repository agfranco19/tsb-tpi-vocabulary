package tsb.vocabulary.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;
import tsb.vocabulary.repositories.VocabularyRepository;

import java.util.List;
import java.util.Map;

@Service
public class VocabularyServiceImpl implements VocabularyService {
    private final VocabularyRepository vocabularyRepository;

    @Autowired
    public VocabularyServiceImpl(VocabularyRepository vocabularyRepository) {
        this.vocabularyRepository = vocabularyRepository;
    }

    @Override
    public boolean existsDocument(Document document) {
        return vocabularyRepository.existsDocument(document);
    }

    public boolean saveProcessedDocument(Document document, Map<String, Integer> words) {
        return vocabularyRepository.saveProcessedDocument(document, words);
    }

    @Override
    public List<Document> listProcessedDocuments() {
        return vocabularyRepository.listDocuments();
    }

    @Override
    public Vocabulary getVocabulary() {
        return vocabularyRepository.getVocabulary();
    }
}
