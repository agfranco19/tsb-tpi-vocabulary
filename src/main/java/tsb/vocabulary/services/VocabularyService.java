package tsb.vocabulary.services;

import org.springframework.stereotype.Service;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;

import java.util.List;
import java.util.Map;

/**
 * Vocabulary services provides the interface needed to process documents and manage an vocabulary or terms.
 */
@Service
public interface VocabularyService {

    /**
     * Check if the document was already processed.
     *
     * @param document document to check if exists
     * @return true - whether the document was processed, false otherwise
     */
    boolean existsDocument(Document document);

    /**
     * Process a new document and updates the vocabulary.
     * If the document was already processed it will be dismissed.
     *
     * @param document the document to process
     * @throws tsb.vocabulary.exceptions.DocumentProcessorException
     */
    boolean saveProcessedDocument(Document document, Map<String, Integer> words);

    /**
     * Return a list with all processed documents and present in the vocabulary.
     *
     * @return a list with all processed documents
     */
    List<Document> listProcessedDocuments();

    Vocabulary getVocabulary();
}
