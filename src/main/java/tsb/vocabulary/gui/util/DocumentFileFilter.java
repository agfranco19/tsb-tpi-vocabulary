package tsb.vocabulary.gui.util;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class DocumentFileFilter extends FileFilter {

    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }

        return (file.isFile() && file.getName().endsWith(".txt"));
    }

    @Override
    public String getDescription() {
        return "Archivos de Texto";
    }
}
