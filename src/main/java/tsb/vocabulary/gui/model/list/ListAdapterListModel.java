package tsb.vocabulary.gui.model.list;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A {@link ListModel} that is based upon a {@link List}.
 *
 * @author Ariel Franco
 */
public class ListAdapterListModel<E> extends AbstractListModel<E> {
    private List<E> list;

    public ListAdapterListModel() {
        list = new ArrayList<>();
    }

    public void setList(List<E> list) {
        this.list.clear();
        this.list.addAll(list);
        fireListDataChanged();
    }

    public void clear() {
        this.list.clear();
        fireListDataChanged();
    }

    private void fireListDataChanged() {
        fireContentsChanged(this, 0, Math.max(list.size() - 1, 0));
    }

    public void addAll(List<E> elements) {
        if (this.list.addAll(elements)) {
            fireIntervalAdded(elements.size());
        }
    }

    public void remove(E element) {
        list.remove(element);
        fireListDataChanged();
    }

    public void removeAll(List<E> elements) {
        if (this.list.removeAll(elements)) {
            fireIntervalRemoved(elements.size());
        }
    }

    protected void fireIntervalRemoved(int elementCount) {
        int index0 = list.size() - elementCount;
        fireIntervalRemoved(this, Math.max(0, index0),
                Math.max(0, list.size() - 1));
    }

    @Override
    protected void fireIntervalRemoved(Object source, int index0, int index1) {
        super.fireIntervalRemoved(source, index0, index1);
    }

    protected void fireIntervalAdded(int elementCount) {
        int index0 = list.size() - elementCount;
        fireIntervalAdded(this, Math.max(0, index0),
                Math.max(0, list.size() - 1));
    }

    /**
     * Returns the list that this {@link ListAdapterListModel} is backed by.
     *
     * @return
     */
    public List<E> getList() {
        return Collections.unmodifiableList(list);
    }

    public int getSize() {
        return list.size();
    }

    public E getElementAt(int index) {
        E elementAt = list.get(index);
        return elementAt;
    }

    public int indexOf(E element) {
        return list.indexOf(element);
    }
}
