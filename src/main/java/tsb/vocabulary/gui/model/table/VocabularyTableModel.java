package tsb.vocabulary.gui.model.table;

import tsb.vocabulary.data.VocabularyTerm;

import javax.swing.table.AbstractTableModel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ariel Franco
 */
public class VocabularyTableModel extends AbstractTableModel {
    private List<VocabularyTerm> terms;
    private Map<Column, String> columnDisplayNames;

    private enum Column {
        WORDS, FREQUENCY, DOCUMENTS
    }

    public VocabularyTableModel() {
        terms = Collections.emptyList();
        columnDisplayNames = new HashMap<>();
        columnDisplayNames.put(Column.WORDS, "Palabras");
        columnDisplayNames.put(Column.FREQUENCY, "Frecuencia");
        columnDisplayNames.put(Column.DOCUMENTS, "Documentos");
    }

    public void setListModel(List<VocabularyTerm> terms) {
        this.terms = terms;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return terms.size();
    }

    @Override
    public int getColumnCount() {
        return Column.values().length;
    }

    @Override
    public String getColumnName(int column) {
        Column columnObj = getColumn(column);
        String displayName = columnDisplayNames.get(columnObj);
        return displayName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object columnValue = null;

        VocabularyTerm term = terms.get(rowIndex);
        Column column = getColumn(columnIndex);

        switch (column) {
            case WORDS:
                columnValue = term.getWord();
                break;
            case FREQUENCY:
                columnValue = term.getFrequency();
                break;
            default:
                columnValue = term.getNumberOfDocuments();
                break;
        }
        return columnValue;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class columnClass = String.class;
        Column column = getColumn(columnIndex);

        switch (column) {
            case FREQUENCY:
            case DOCUMENTS:
                columnClass = Integer.class;
                break;

        }
        return columnClass;
    }

    private Column getColumn(int columnIndex) {
        Column[] columns = Column.values();
        Column column = columns[columnIndex];
        return column;
    }
}
