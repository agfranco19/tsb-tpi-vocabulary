package tsb.vocabulary.gui.controller;

import java.util.EventListener;

/**
 * @author Ariel Franco
 */
public interface ProcessDocumentListener extends EventListener {

    void onDocumentToProcess(DocumentToProcessEvent evt);
}
