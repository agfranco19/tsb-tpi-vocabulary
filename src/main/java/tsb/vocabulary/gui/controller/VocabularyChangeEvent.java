package tsb.vocabulary.gui.controller;

import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;

import java.util.EventObject;
import java.util.List;

/**
 * @author Ariel Franco
 */
public class VocabularyChangeEvent extends EventObject {
    private Vocabulary vocabulary;
    private List<Document> processedDocuments;
    private List<Document> documentsQueue;

    /**
     * Constructs a prototypical Event.
     *
     * @param source     The object on which the Event initially occurred.
     * @param vocabulary
     * @param documentsQueue
     * @throws IllegalArgumentException if source is null.
     */
    public VocabularyChangeEvent(Object source, Vocabulary vocabulary, List<Document> processedDocuments, List<Document> documentsQueue) {
        super(source);
        this.vocabulary = vocabulary;
        this.processedDocuments = processedDocuments;
        this.documentsQueue = documentsQueue;
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public List<Document> getProcessedDocuments() {
        return processedDocuments;
    }

    public List<Document> getDocumentsQueue() {
        return documentsQueue;
    }
}
