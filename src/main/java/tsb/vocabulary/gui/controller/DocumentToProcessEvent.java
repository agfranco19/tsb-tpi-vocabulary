package tsb.vocabulary.gui.controller;

import java.util.EventObject;

/**
 * @author Ariel Franco
 */
public class DocumentToProcessEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public DocumentToProcessEvent(Object source) {
        super(source);
    }
}
