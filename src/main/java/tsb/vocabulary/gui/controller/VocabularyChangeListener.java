package tsb.vocabulary.gui.controller;

import java.util.EventListener;

/**
 * @author Ariel Franco
 */
public interface VocabularyChangeListener extends EventListener {

    void onVocabularyChange(VocabularyChangeEvent evt);
}
