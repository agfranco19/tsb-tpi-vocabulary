package tsb.vocabulary.gui.controller;

/**
 * @author Ariel Franco
 */
public interface SearchFilterTextListener {

    void onSearchFilterTextChange(SearchFilterTextChangeEvent evt);
}
