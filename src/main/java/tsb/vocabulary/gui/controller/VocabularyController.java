package tsb.vocabulary.gui.controller;

import org.jdesktop.swingx.sort.RowFilters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;
import tsb.vocabulary.gui.util.DocumentFileFilter;
import tsb.vocabulary.gui.view.DocumentsPanel;
import tsb.vocabulary.gui.view.VocabularyPanel;
import tsb.vocabulary.processor.DocumentProcessor;
import tsb.vocabulary.services.VocabularyService;

import javax.swing.*;
import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

public class VocabularyController implements SearchFilterTextListener, ProcessDocumentListener, VocabularyChangeListener {
    private Vocabulary vocabulary;
    private final VocabularyService vocabularyService;
    private final DocumentProcessor documentProcessor;
    private VocabularyChangeListener vocabularyChangeListener;
    // UI components
    private final DocumentsPanel documentsPanel;
    private final VocabularyPanel vocabularyPanel;
    private final JProgressBar progressBar;

    private static final Logger log = LoggerFactory.getLogger(VocabularyController.class);

    public VocabularyController(VocabularyService vocabularyService, DocumentProcessor documentProcessor,
                                DocumentsPanel documentsPanel, VocabularyPanel vocabularyPanel, JProgressBar progressBar) {
        this.vocabularyService = vocabularyService;
        this.documentProcessor = documentProcessor;
        this.documentsPanel = documentsPanel;
        this.vocabularyPanel = vocabularyPanel;
        this.progressBar = progressBar;

        vocabulary = vocabularyService.getVocabulary();

        this.documentsPanel.setProcessedDocumentsList(vocabularyService.listProcessedDocuments());
        this.documentsPanel.setDocumentsQueueList(Collections.emptyList());
        this.documentsPanel.addDocumentToProcessListener(this);

        this.vocabularyPanel.setVocabularyTerms(vocabulary.getVocabularyTerms());
        this.vocabularyPanel.addSearchFilterTextChangeListener(this);

        this.addVocabularyChangeListener(this);
    }

    public void addVocabularyChangeListener(VocabularyChangeListener listener) {
        vocabularyChangeListener = listener;
    }

    @Override
    public void onSearchFilterTextChange(SearchFilterTextChangeEvent evt) {
        RowFilter rowFilter;
        Pattern pattern = Pattern.compile("^" + evt.getFilterText());
        try {
            rowFilter = RowFilters.regexFilter(pattern);
        } catch (java.util.regex.PatternSyntaxException e) {
            log.debug(e.getMessage(), e);
            return;
        }
        vocabularyPanel.setVocabularyFilter(rowFilter);
    }

    @Override
    public void onDocumentToProcess(DocumentToProcessEvent evt) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setFileFilter(new DocumentFileFilter());

        int result = fileChooser.showDialog(documentsPanel, "Cargar archivo");
        if (result != JFileChooser.CANCEL_OPTION) {
            File[] selectedFiles = fileChooser.getSelectedFiles();
            processDocuments(selectedFiles);
        }
    }

    private void processDocuments(File[] files) {
        List<Document> documentsToProcess = new ArrayList<>();
        Arrays.stream(files).forEach(file -> documentsToProcess.add(new Document(file)));
        documentsPanel.setDocumentsQueueList(documentsToProcess);

        new BackgroundTaskWorker(documentsToProcess).execute();
    }

    private void fireVocabularyChangeEvent(VocabularyChangeEvent evt) {
        this.vocabularyChangeListener.onVocabularyChange(evt);
    }

    @Override
    public void onVocabularyChange(VocabularyChangeEvent evt) {
        log.info("Change in vocabulary");
        documentsPanel.setProcessedDocumentsList(evt.getProcessedDocuments());
        documentsPanel.setDocumentsQueueList(evt.getDocumentsQueue());
        vocabularyPanel.setVocabularyTerms(evt.getVocabulary().getVocabularyTerms());
    }

    private class BackgroundTaskWorker extends SwingWorker<Void, Integer> {
        private List<Document> documentsToProcess;

        private BackgroundTaskWorker(List<Document> documentsToProcess) {
            this.documentsToProcess = documentsToProcess;
        }

        @Override
        protected Void doInBackground() throws Exception {
            progressBar.setVisible(true);
            progressBar.setMaximum(documentsToProcess.size());
            int current = 0;

            ListIterator<Document> listIterator = documentsToProcess.listIterator();
            while (listIterator.hasNext()) {
                current++;
                Document document = listIterator.next();
                if (!vocabularyService.existsDocument(document)) {
                    progressBar.setString("Processing " + document.getName());

                    String path = document.getDocumentFile().getAbsolutePath();
                    Map<String, Integer> vocabularyTerms = documentProcessor.processDocument(path);
                    boolean processedSuccessfully = vocabularyService.saveProcessedDocument(document, vocabularyTerms);

                    if (processedSuccessfully) {
                        Vocabulary vocabulary = vocabularyService.getVocabulary();
                        List<Document> processedDocuments = vocabularyService.listProcessedDocuments();
                        listIterator.remove();

                        VocabularyChangeEvent evt = new VocabularyChangeEvent(this, vocabulary, processedDocuments, documentsToProcess);
                        fireVocabularyChangeEvent(evt);

                        // updates progressBar
                        publish(current);
                    }
                }
                Thread.sleep(100);
            }
            return null;
        }

        @Override
        protected void done() {
            progressBar.setVisible(false);
        }

        @Override
        protected void process(List<Integer> chunks) {
            for (Integer chunk : chunks) {
                progressBar.setValue(chunk);
            }
        }
    }
}
