package tsb.vocabulary.gui.controller;

import java.util.EventObject;

/**
 * @author Ariel Franco
 */
public class SearchFilterTextChangeEvent extends EventObject {
    private String filterText;

    /**
     * Constructs a prototypical Event.
     *
     * @param source     The object on which the Event initially occurred.
     * @param filterText The filters to filter data in a table
     * @throws IllegalArgumentException if source is null.
     */
    public SearchFilterTextChangeEvent(Object source, String filterText) {
        super(source);
        this.filterText = filterText;
    }

    public String getFilterText() {
        return filterText;
    }
}
