package tsb.vocabulary.gui.view;

import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXPanel;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.gui.controller.DocumentToProcessEvent;
import tsb.vocabulary.gui.controller.ProcessDocumentListener;
import tsb.vocabulary.gui.controller.VocabularyChangeListener;
import tsb.vocabulary.gui.model.list.ListAdapterListModel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class DocumentsPanel extends JXPanel {
    private JXPanel processedDocumentsPanel;
    private JList<Document> processedDocumentsList;
    private JXPanel documentsQueuePanel;
    private JList<Document> documentsQueueList;
    private JXButton processDocumentButton;
    private java.util.List<ProcessDocumentListener> processDocumentListeners;
    private java.util.List<VocabularyChangeListener> vocabularyChangeListeners;

    public DocumentsPanel() {
        processedDocumentsList = new JList<>(new ListAdapterListModel<>());
        processedDocumentsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        documentsQueueList = new JList<>(new ListAdapterListModel<>());
        documentsQueueList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        processDocumentListeners = new LinkedList<>();
        vocabularyChangeListeners = new LinkedList<>();

        processDocumentButton = new JXButton("Agregar documentos...");
        processDocumentButton.addActionListener(l -> fireProcessDocument());

        processedDocumentsPanel = new JXPanel();
        processedDocumentsPanel.setLayout(new BorderLayout());
        processedDocumentsPanel.add(new JScrollPane(processedDocumentsList), BorderLayout.CENTER);
        processedDocumentsPanel.setBorder(BorderFactory.createTitledBorder("Documentos procesados"));

        documentsQueuePanel = new JXPanel();
        documentsQueuePanel.setLayout(new BorderLayout());

        documentsQueuePanel.add(new JScrollPane(documentsQueueList), BorderLayout.CENTER);
        documentsQueuePanel.add(processDocumentButton, BorderLayout.SOUTH);
        documentsQueuePanel.setBorder(BorderFactory.createTitledBorder("Cola de documentos"));

        setLayout(new BorderLayout());
        add(processedDocumentsPanel, BorderLayout.CENTER);
        add(documentsQueuePanel, BorderLayout.SOUTH);
    }

    private void fireProcessDocument() {
        java.util.List<Document> documentsToProcess = new ArrayList<>();
        for (int i = 0; i < documentsQueueList.getModel().getSize(); i++) {
            documentsToProcess.add(documentsQueueList.getModel().getElementAt(i));
        }

        DocumentToProcessEvent evt = new DocumentToProcessEvent(this);
        processDocumentListeners.forEach(listener -> listener.onDocumentToProcess(evt));
    }

    public void setDocumentsQueueList(java.util.List<Document> documentsQueue) {
        ListAdapterListModel<Document> model = new ListAdapterListModel<>();
        model.setList(documentsQueue);
        documentsQueueList.setModel(model);
    }

    public void setProcessedDocumentsList(java.util.List<Document> processedDocuments) {
        ListAdapterListModel<Document> model = new ListAdapterListModel<>();
        model.setList(processedDocuments);
        processedDocumentsList.setModel(model);
    }

    public void addDocumentToProcessListener(ProcessDocumentListener listener) {
        processDocumentListeners.add(listener);
    }

    public void removeProcessDocumentListener(ProcessDocumentListener listener) {
        processDocumentListeners.remove(listener);
    }
}