package tsb.vocabulary.gui.view;

import org.jdesktop.swingx.JXFrame;
import tsb.vocabulary.gui.controller.VocabularyController;
import tsb.vocabulary.processor.DocumentProcessor;
import tsb.vocabulary.services.VocabularyService;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JXFrame {
    private DocumentsPanel documentsPanel;
    private VocabularyPanel vocabularyPanel;
    private JProgressBar progressBar;
    private final VocabularyController controller;

    public MainWindow(VocabularyService vocabularyService, DocumentProcessor documentProcessor) {
        super("Vocabulary");

        documentsPanel = new DocumentsPanel();
        vocabularyPanel = new VocabularyPanel();

        progressBar = new JProgressBar(JProgressBar.HORIZONTAL);
        progressBar.setStringPainted(true);
        progressBar.setVisible(false);

        controller = new VocabularyController(vocabularyService, documentProcessor, documentsPanel, vocabularyPanel, progressBar);

        Dimension preferredSize = getPreferredSizeDimension();
        setSize(preferredSize);

        setLayout(new BorderLayout());
        add(progressBar, BorderLayout.SOUTH);
        add(documentsPanel, BorderLayout.EAST);
        add(vocabularyPanel, BorderLayout.CENTER);
    }

    private static Dimension getPreferredSizeDimension() {
        // get screen dimensions
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenHeight = screenSize.height;
        int screenWidth = screenSize.width;

        return new Dimension(screenWidth / 2, screenHeight / 2);
    }
}