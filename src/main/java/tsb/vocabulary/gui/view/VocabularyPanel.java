package tsb.vocabulary.gui.view;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXTable;
import tsb.vocabulary.gui.controller.SearchFilterTextChangeEvent;
import tsb.vocabulary.gui.controller.SearchFilterTextListener;
import tsb.vocabulary.data.VocabularyTerm;
import tsb.vocabulary.gui.model.table.VocabularyTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;

/**
 * @author Ariel Franco
 */
public class VocabularyPanel extends JXPanel {
    private JXTable vocabularyTable;
    private VocabularyTableModel vocabularyTableModel;
    private JXLabel searchFilterLabel;
    private JXSearchField searchFilterText;
    private JXPanel searchFilterPanel;
    private java.util.List<SearchFilterTextListener> searchFilterTextListeners;

    public VocabularyPanel() {
        vocabularyTableModel = new VocabularyTableModel();
        vocabularyTable = new JXTable(vocabularyTableModel);

        searchFilterLabel = new JXLabel("Palabra:");
        searchFilterText = new JXSearchField("Buscar");
        searchFilterText.setMaximumSize(new Dimension(110, 20));
        searchFilterText.setPreferredSize(new Dimension(250, 20));

        searchFilterPanel = new JXPanel();
        searchFilterPanel.setLayout(new FlowLayout());
        searchFilterPanel.add(searchFilterLabel);
        searchFilterPanel.add(searchFilterText);

        searchFilterTextListeners = new LinkedList<>();

        searchFilterText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                fireSearchFilterChange();
            }

            @Override
            public void keyPressed(KeyEvent e) {
                fireSearchFilterChange();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                fireSearchFilterChange();
            }
        });

        setLayout(new BorderLayout());
        add(new JScrollPane(vocabularyTable), BorderLayout.CENTER);
        add(searchFilterPanel, BorderLayout.NORTH);
    }

    private void fireSearchFilterChange() {
        SearchFilterTextChangeEvent evt = new SearchFilterTextChangeEvent(this, searchFilterText.getText().toUpperCase());
        searchFilterTextListeners.forEach(listener -> listener.onSearchFilterTextChange(evt));
    }

    public void setVocabularyTerms(java.util.List<VocabularyTerm> vocabularyListModel) {
        vocabularyTableModel.setListModel(vocabularyListModel);
    }

    public void addSearchFilterTextChangeListener(SearchFilterTextListener listener) {
        searchFilterTextListeners.add(listener);
    }

    public void removeSearchFilterTextListener(SearchFilterTextListener listener) {
        searchFilterTextListeners.remove(listener);
    }

    public void setVocabularyFilter(RowFilter vocabularyFilter) {
        this.vocabularyTable.setRowFilter(vocabularyFilter);
    }
}