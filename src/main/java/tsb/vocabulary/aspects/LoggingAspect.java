package tsb.vocabulary.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * Aspect for logging different kind of events.
 */
@Aspect
@Component
public class LoggingAspect {
    private static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Around("execution(* tsb.vocabulary.processor.*.processDocument(String))")
    public Object callProcessDocumentBefore(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        StopWatch watch = new StopWatch();
        watch.start();

        String document = proceedingJoinPoint.getArgs()[0].toString();
        Object returnVal = proceedingJoinPoint.proceed();

        watch.stop();
        logger.info(String.format("Processed document ### %s ### (elapsed time: %d ms).", document, watch.getTotalTimeMillis()));

        return returnVal;
    }
}
