package tsb.vocabulary.data;

import java.util.Objects;

public class Word {
    private final String name;
    private Long id;

    public Word(String name) {
        this.name = name;
    }

    public Word(String name, Long id) {
        this(name);
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Word that = (Word) obj;
        return Objects.equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
