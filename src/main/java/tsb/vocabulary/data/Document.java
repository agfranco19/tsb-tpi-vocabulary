package tsb.vocabulary.data;

import java.io.File;
import java.util.Objects;

public class Document {
    private final String name;
    private File documentFile;
    private Long id;

    public Document(String name) {
        this.name = name;
    }

    public Document(String name, Long id) {
        this(name);
        this.id = id;
    }

    public Document(File documentFile) {
        this(documentFile.getName());
        this.documentFile = documentFile;
    }

    public String getName() {
        return name;
    }

    public File getDocumentFile() {
        return documentFile;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Document that = (Document) obj;
        return Objects.equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
