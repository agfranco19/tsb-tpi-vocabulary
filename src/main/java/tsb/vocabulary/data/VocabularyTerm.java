package tsb.vocabulary.data;

import java.util.Objects;

public class VocabularyTerm {
    private String word;
    private int numberOfDocuments;
    private int frequency;

    public VocabularyTerm(String word, int numberOfDocuments, int frequency) {
        this.word = word;
        this.numberOfDocuments = numberOfDocuments;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public int getNumberOfDocuments() {
        return numberOfDocuments;
    }

    public int getFrequency() {
        return frequency;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        VocabularyTerm that = (VocabularyTerm) obj;
        return Objects.equals(this.word, that.word);
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s(%d,%d)", word, frequency, numberOfDocuments);
    }
}
