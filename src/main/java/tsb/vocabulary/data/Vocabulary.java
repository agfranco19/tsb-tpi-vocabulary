package tsb.vocabulary.data;

import java.util.List;

public class Vocabulary {
    private final List<VocabularyTerm> vocabularyTerms;

    public Vocabulary(List<VocabularyTerm> vocabularyTerms) {
        this.vocabularyTerms = vocabularyTerms;
    }

    public List<VocabularyTerm> getVocabularyTerms() {
        return vocabularyTerms;
    }
}
