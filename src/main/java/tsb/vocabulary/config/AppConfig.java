package tsb.vocabulary.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;
import tsb.vocabulary.gui.view.MainWindow;
import tsb.vocabulary.processor.DocumentProcessor;
import tsb.vocabulary.services.VocabularyService;

import javax.sql.DataSource;

@Configuration
public class AppConfig {
    @Value("${db.driver.production}")
    private String dbDriver;

    @Value("${db.url.production}")
    private String dbUrl;

    @Profile("prod")
    @Bean(name = "dataSource")
    public DataSource dataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(dbDriver);
        ds.setUrl(dbUrl);
        ds.setInitialSize(5);
        ds.setMaxTotal(10);
        return ds;
    }

    @Profile("prod")
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Profile({"test", "dev"})
    @Bean(name = "dataSource", destroyMethod = "shutdown")
    public DataSource dataSourceForTest() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScript("schema.sql")
                .addScripts("data.sql")
                .build();
    }

    @Profile({"test", "dev"})
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManagerForTest() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Profile({"prod", "dev"})
    @Bean
    @Autowired
    public MainWindow mainWindow(VocabularyService vocabularyService, DocumentProcessor documentProcessor) {
        return new MainWindow(vocabularyService, documentProcessor);
    }
}
