package tsb.vocabulary.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;
import tsb.vocabulary.data.VocabularyTerm;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

@Repository
public class VocabularyRepositoryImpl implements VocabularyRepository {
    private JdbcTemplate jdbcTemplate;
    private DataSource dataSource;
    private Map<String, Long> processedDocuments;
    private Map<String, Long> vocabularyWords;
    public static final double BATCH_SIZE = 1000;

    private static Logger log = LoggerFactory.getLogger(VocabularyRepositoryImpl.class);

    @Autowired
    public VocabularyRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.dataSource = dataSource;
        loadCacheFromDB();
    }

    @Override
    public List<Document> listDocuments() {
        final String SELECT_SQL = "SELECT document_name FROM documents";
        List<Document> documents = jdbcTemplate.query(SELECT_SQL,
                (rs, rowNum) -> new Document(rs.getString(1)));
        return documents;
    }

    @Override
    public int getNumberOfDocuments() {
        return processedDocuments.size();
    }

    @Override
    public Vocabulary getVocabulary() {
        final String SELECT_SQL = "SELECT w.word_name, COUNT(v.document_id), SUM(v.frequency) " +
                "FROM words w INNER JOIN vocabulary v ON (w.id = v.word_id) " +
                "GROUP BY w.word_name " +
                "ORDER BY w.word_name";

        List<VocabularyTerm> vocabularyTerms = jdbcTemplate.query(SELECT_SQL,
                (rs, rowNum) -> new VocabularyTerm(rs.getString("word_name"), rs.getInt(2), rs.getInt(3)));

        return new Vocabulary(vocabularyTerms);
    }

    @Override
    public boolean existsDocument(Document document) {
        return processedDocuments.containsKey(document.getName());
    }

    private boolean existsWord(String word) {
        return vocabularyWords.containsKey(word);
    }

    @Override
    public boolean saveProcessedDocument(Document document, Map<String, Integer> words) {
        if (existsDocument(document)) {
            log.debug("Document already processed...");
            return false;
        }

        boolean documentStored = false;

        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement documentPreparedStatement = createInsertDocumentPreparedStatement(connection, document);
                 PreparedStatement wordsPreparedStatement = createInsertWordsPreparedStatement(connection, words);
                 PreparedStatement vocabularyTermsPreparedStatement = createInsertVocabularyTermsPreparedStatement(connection, document, words)) {

                documentStored = true;
            } catch (SQLException ex) {
                connection.rollback();
                connection.setAutoCommit(true);
                log.error("SQL Exception - Error code: " + ex.getErrorCode());
                log.debug(ex.getMessage(), ex);
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return documentStored;
    }

    private PreparedStatement createInsertDocumentPreparedStatement(Connection connection, Document document) throws SQLException {
        final String INSERT_DOCUMENT_SQL = "INSERT INTO documents(document_name) VALUES(?)";
        PreparedStatement statement = connection.prepareStatement(INSERT_DOCUMENT_SQL, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, document.getName());
        statement.executeUpdate();

        try (ResultSet resultSet = statement.getGeneratedKeys()) {
            if (resultSet != null && resultSet.next()) {
                document.setId(resultSet.getLong(1));
            }
        }
        loadProcessedDocumentsFromDB(connection);
        return statement;
    }

    private PreparedStatement createInsertWordsPreparedStatement(Connection connection, Map<String, Integer> words) throws SQLException {
        final String INSERT_WORD_SQL = "INSERT INTO words(word_name) VALUES(?)";
        PreparedStatement statement = connection.prepareStatement(INSERT_WORD_SQL);

        int insertCounter = 0;
        Set<String> keys = words.keySet();

        Iterator<String> keysIterator = keys.iterator();
        while (keysIterator.hasNext()) {
            String word = keysIterator.next();
            if (existsWord(word)) {
                continue;
            }

            statement.setString(1, word);
            statement.addBatch();
            insertCounter++;
            vocabularyWords.put(word, null);

            if (insertCounter % BATCH_SIZE == 0) {
                log.debug("Executing batch.");
                statement.executeBatch();
            }
        }
        statement.executeBatch();
        // update word IDs
        loadVocabularyWordsFromDB(connection);
        return statement;
    }

    private PreparedStatement createInsertVocabularyTermsPreparedStatement(Connection connection, Document document, Map<String, Integer> words) throws SQLException {
        final String INSERT_TERM_SQL = "INSERT INTO vocabulary(word_id, document_id, frequency) VALUES(?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(INSERT_TERM_SQL);

        int insertCounter = 0;
        Set<String> keys = words.keySet();

        Iterator<String> keysIterator = keys.iterator();
        while (keysIterator.hasNext()) {
            String word = keysIterator.next();
            Long wordId = vocabularyWords.get(word);
            int frequency = words.get(word);

            statement.setLong(1, wordId);
            statement.setLong(2, document.getId());
            statement.setLong(3, frequency);
            statement.addBatch();
            insertCounter++;

            if (insertCounter % BATCH_SIZE == 0) {
                log.debug("Executing batch.");
                statement.executeBatch();
            }
        }
        statement.executeBatch();
        return statement;
    }

    private void loadCacheFromDB() {
        try (Connection connection = dataSource.getConnection()) {
            loadProcessedDocumentsFromDB(connection);
            loadVocabularyWordsFromDB(connection);
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private void loadProcessedDocumentsFromDB(Connection connection) throws SQLException {
        this.processedDocuments = new HashMap<>();

        final String SELECT_SQL = "SELECT document_name, id FROM documents";
        try (PreparedStatement statement = connection.prepareStatement(SELECT_SQL);
             ResultSet rs = statement.executeQuery()) {
            while (rs.next()) {
                String documentName = rs.getString(1);
                Long documentId = rs.getLong(2);
                this.processedDocuments.put(documentName, documentId);
            }
        }
    }

    private void loadVocabularyWordsFromDB(Connection connection) throws SQLException {
        this.vocabularyWords = new HashMap<>();

        final String SELECT_SQL = "SELECT word_name, id FROM words";
        try (PreparedStatement statement = connection.prepareStatement(SELECT_SQL);
             ResultSet rs = statement.executeQuery()) {
            while (rs.next()) {
                String wordName = rs.getString(1);
                Long wordId = rs.getLong(2);
                this.vocabularyWords.put(wordName, wordId);
            }
        }
    }
}
