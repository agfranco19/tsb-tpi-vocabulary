package tsb.vocabulary.repositories;

import org.springframework.stereotype.Repository;
import tsb.vocabulary.data.Document;
import tsb.vocabulary.data.Vocabulary;

import java.util.List;
import java.util.Map;

@Repository
public interface VocabularyRepository {

    /**
     * List all processed documents.
     *
     * @return a list with all processed documents
     */
    List<Document> listDocuments();

    /**
     * Insert a new processed document. If the document already exists it will be dismissed.
     *
     * @param document the document to insert
     * @return true - if was added successfully, false - otherwise
     */
    boolean saveProcessedDocument(Document document, Map<String, Integer> words);

    /**
     * Indicates if a document was already processed.
     *
     * @param document the document to check if it exists
     * @return true - whether the document was processed, false - otherwise
     */
    boolean existsDocument(Document document);

    /**
     * Return the number of processed documents.
     *
     * @return the number of processed documents
     */
    int getNumberOfDocuments();

    Vocabulary getVocabulary();
}
