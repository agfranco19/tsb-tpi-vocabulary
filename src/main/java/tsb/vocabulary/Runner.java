package tsb.vocabulary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import tsb.vocabulary.gui.view.MainWindow;

import javax.swing.*;

@Profile({"prod", "dev"})
@Component
public class Runner implements CommandLineRunner {
    @Autowired
    private MainWindow mainWindow;

    private static Logger logger = LoggerFactory.getLogger(Runner.class);

    @Override
    public void run(String... args) throws Exception {
        setLookAndFell();
        SwingUtilities.invokeLater(() -> {
            mainWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            mainWindow.setVisible(true);
        });
    }

    private static void setLookAndFell() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | javax.swing.UnsupportedLookAndFeelException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
