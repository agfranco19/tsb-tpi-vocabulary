package tsb.vocabulary.processor;

import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * A factory to create scanners that allow process a document and return all tokens in it.
 */
@Component
public interface DocumentScannerFactory {

    /**
     * Creates a @{@link java.util.Scanner} that reads a given document using default getCharset.
     *
     * @param document the document that the scanner will read
     * @return an instance of @{@link java.util.Scanner}
     * @throws IllegalArgumentException if document is not valid
     */
    default Scanner createScanner(String document) {
        return new Scanner(document);
    }

    /**
     * Creates a @{@link java.util.Scanner} that reads a given document using a specific getCharset.
     *
     * @param document the document that the scanner will read
     * @param charset  the getCharset used for the scanner when reading the document
     * @return an instance of @{@link java.util.Scanner}
     * @throws IllegalArgumentException if document is not valid
     */
    default Scanner createScanner(String document, String charset) {
        return createScanner(document);
    }
}

