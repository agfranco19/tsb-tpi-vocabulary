package tsb.vocabulary.processor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProcessorRulesConfigurationImpl implements ProcessorRulesConfiguration {
    private final String regex;
    private final String charset;

    ProcessorRulesConfigurationImpl(@Value("${processor.rule.regex}") String regex, @Value("${processor.rule.charset}") String charset) {
        this.regex = regex;
        this.charset = charset;
    }

    @Override
    public String regex() {
        return regex;
    }

    @Override
    public String charset() {
        return charset;
    }
}
