package tsb.vocabulary.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

@Component
public class DocumentProcessorImpl implements DocumentProcessor {
    private ProcessorRulesConfiguration processorRulesConfiguration;
    private DocumentScannerFactory documentScannerFactory;

    @Autowired
    public DocumentProcessorImpl(ProcessorRulesConfiguration processorRulesConfiguration, DocumentScannerFactory documentScannerFactory) {
        this.processorRulesConfiguration = processorRulesConfiguration;
        this.documentScannerFactory = documentScannerFactory;
    }

    @Override
    public Map<String, Integer> processDocument(String document) {
        Map<String, Integer> map = new HashMap<>();
        Scanner scanner = documentScannerFactory.createScanner(document, processorRulesConfiguration.charset());
        scanner.useDelimiter(Pattern.compile(processorRulesConfiguration.regex()));

        while (scanner.hasNext()) {
            String aux = scanner.next().toUpperCase();
            if (aux.length() > 1) {
                if (!map.containsKey(aux)) {
                    map.put(aux, 1);
                } else {
                    Integer frequency = map.get(aux);
                    frequency += 1;
                    map.put(aux, frequency);
                }
            }
        }

        scanner.close();
        return map;
    }
}
