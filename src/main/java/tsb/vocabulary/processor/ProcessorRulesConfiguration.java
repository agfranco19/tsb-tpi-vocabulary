package tsb.vocabulary.processor;

import org.springframework.stereotype.Component;

/**
 * A processor rules configuration allow set specific rules for a document processor.
 */
@Component
public interface ProcessorRulesConfiguration {

    /**
     * A regex to filter tokens when processing a document.
     *
     * @return a regex to filter tokens
     */
    String regex();

    /**
     * The charset encoding of the document.
     *
     * @return the name of the charset
     */
    String charset();
}
