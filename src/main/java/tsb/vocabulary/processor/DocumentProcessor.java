package tsb.vocabulary.processor;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * A document processor process a document and returns all the different tokens found.
 */
@Component
public interface DocumentProcessor {

    /**
     * Process a document and return a map with each word and its frequency in the document.
     *
     * @param document the document to process
     * @return a map with all alphabetical words and their frequency of apparition
     */
    Map<String, Integer> processDocument(String document);

}
