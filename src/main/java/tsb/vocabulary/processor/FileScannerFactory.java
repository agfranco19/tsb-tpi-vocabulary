package tsb.vocabulary.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * A factory to create scanners associated with a document file.
 * The scanner allows process a text document and return all tokens in it.
 */
@Component
public class FileScannerFactory implements DocumentScannerFactory {
    private static Logger logger = LoggerFactory.getLogger(FileScannerFactory.class);

    @Override
    public Scanner createScanner(String document) {
        return createScanner(document, Charset.defaultCharset().name());
    }

    @Override
    public Scanner createScanner(String document, String charset) {
        final String documentNotValidMessage = "Document must be a valid text file.";
        if (document == null) throw new IllegalArgumentException(documentNotValidMessage);

        try {
            Scanner scanner = new Scanner(new File(document), charset);
            return scanner;
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
            throw new IllegalArgumentException(documentNotValidMessage);
        }
    }
}
